function [ret_val, theta] = generate_truth(X, degree, min_coef, max_coef )
    theta = rand(degree+1, 1)*(max_coef-min_coef) + min_coef;
    ret_val = polyval(theta, X);
    ret_val = ret_val';
    
    % scale
    max_y = max(ret_val);
    min_y = min(ret_val);
    ret_val = (ret_val - min_y)/(max_y - min_y);
end


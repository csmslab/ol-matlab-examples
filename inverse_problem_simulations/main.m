clear;

% simulation loop sizes
NUM_SIMS = 20;
NUM_SAMPLES_FOR_PRIORS = 5000;
NUM_EXPERIMENTS = 20;
NUM_MC_STEPS = 20;

% measurement noise levels
SIGMAW = [0.1, 0.25, 0.5];

% alternatives
MIN_X = 0;
MAX_X = 1;
STEP_X = 0.05;
X = MIN_X:STEP_X:MAX_X;

% truth generation
DEGREE = 2;
MIN_COEF = -1;
MAX_COEF = 1;
generate_truth_parameterized = @(X) ...
    generate_truth(X, DEGREE, MIN_COEF, MAX_COEF);

% Policies:
policy_klla_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_klla(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS);

% Define quantities of interest (qoi) for KG policies
% --- Global QOI - the response vector itself.
qoi_global = @(mu) mu;
% --- Inverse Problem QOI - the solution to an inverse problem.
qoi_inverse_poly_parameterized = @(mu) ...
    qoi_inverse_poly(mu, DEGREE, X);

% Define weighted norm by z-normalization
theta = zeros(NUM_SAMPLES_FOR_PRIORS, DEGREE+1);
for i=1:NUM_SAMPLES_FOR_PRIORS
    Y = generate_truth_parameterized(X);
    theta(i, :) = qoi_inverse_poly_parameterized(Y);
end
mu_theta = mean(theta);
sig_theta = std(theta);

% Distance for Inverse Problem QOI
theta_normalization = @(theta) (theta-mu_theta)./sig_theta;
qoi_normalized_theta_norm = @(theta) norm(theta_normalization(theta));
qoi_dist_norm = @(qoi1, qoi2) qoi_normalized_theta_norm(qoi1-qoi2);

% Inverse problem KG policy
policy_inverse_poly_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_inverse_poly_parameterized, qoi_dist_norm);
    
% Global KG Policy
policy_global_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_global, @(qoi1, qoi2) norm(qoi1 - qoi2));
 
% A cell array of all policies to consider
POLICY_FNCS = {
    @policy_exploration
    @policy_max_var
    policy_inverse_poly_kg
    %policy_global_kg
};

% A label for the policies we are considering
POLICY_LABELS = {
    'Exploration'
    'Max Variance'
    'Inverse-Problem KG'
    %'KG (Global)'
};


% ------------------------- Run simulations- ------------------------------
        
[truth, observations, mu, Sigma, choices, prior_samples] = simulate(...
    NUM_SIMS, NUM_SAMPLES_FOR_PRIORS, NUM_EXPERIMENTS, ...
    POLICY_FNCS, SIGMAW, generate_truth_parameterized, X);


% ------------------------ Calculate metrics ------------------------------

% normalized error metric based on how well we find inverse problem QOI
perf_qoi_parameterized = @(mu, truth) ...
    perf_normed_qoi_rel_err(mu, truth, qoi_inverse_poly_parameterized, ...
        qoi_normalized_theta_norm);
err = calc_perf(mu, truth, perf_qoi_parameterized);

% unnormalized error metric based on how well we find inverse problem QOI
perf_qoi_unnormalized = @(mu, truth) ...
    perf_normed_qoi_rel_err(mu, truth, qoi_inverse_poly_parameterized, ...
        @norm);
err_unnormalized = calc_perf(mu, truth, perf_qoi_unnormalized);

% ---------------------------- Make plots ---------------------------------

for noise_idx=1:length(SIGMAW)
 
    % plot normalized error metric
    make_perf_plots( squeeze(err(:,noise_idx,:,:)), POLICY_LABELS, ...
        sprintf('Parameter Error, \\sigma_w = %4.2f', ...
            SIGMAW(noise_idx)),...
        sprintf('param.sigmaw_%4.2f', SIGMAW(noise_idx)),...
        'Relative Error');
    
    % plot unnormalized error metric
    make_perf_plots( squeeze(err_unnormalized(:,noise_idx,:,:)), ...
        POLICY_LABELS, ...
        sprintf('Parameter Error (Unnormalized), \\sigma_w = %4.2f', ...
            SIGMAW(noise_idx)),...
        sprintf('param.unnormalized.sigmaw_%4.2f', SIGMAW(noise_idx)),...
        'Relative Error (Unnormalized)');
end


% save all simulation results
save('sim_results.mat', '-v7.3');



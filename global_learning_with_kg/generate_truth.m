
function ret_val = generate_truth(X, n)

ret_val = ones(size(X));

min_x = min(X);
max_x = max(X);


% generate n roots
%roots = rand(1,n)*(max_x - min_x) + min_x;

% form response
%for i=1:n
%    ret_val = ret_val.*(X - roots(i));
%end


% generate centers
centers = rand(1,n)*(max_x - min_x) + min_x;

% form response
sig = 0.1*(max_x-min_x);

for i=1:(n-1)
    ret_val = ret_val + exp(-(X-centers(i)).^2/(2.0*sig*sig));
end

% scale
min_y = min(ret_val);
max_y = max(ret_val);
ret_val = (ret_val-min_y)/(max_y-min_y);



end
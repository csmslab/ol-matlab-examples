clear;

% simulation loop sizes
NUM_SIMS = 20;
NUM_SAMPLES_FOR_PRIORS = 1000;
NUM_EXPERIMENTS = 20;
NUM_MC_STEPS = 20;

% noise levels
SIGMAW = [0.4 0.6];

% alternatives
MIN_X = 0;
MAX_X = 1;
STEP_X = 0.01;
X = MIN_X:STEP_X:MAX_X;

% truth generation
NUM_MODES = 2;
generate_truth_parameterized = @(X) ...
    generate_truth(X, NUM_MODES);

% Policies:

policy_klla_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_klla(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS);

qoi_global = @(mu) mu;
qoi_extrema_parameterized = @(mu) ...
    qoi_extrema_1D(mu, NUM_MODES, X);
qoi_target_parameterized = @(mu) ...
    qoi_arg_target(mu, 1);
qoi_dist = @(y1, y2) ...
    norm(y1 - y2);

policy_extrema_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_extrema_parameterized, qoi_dist);
    
policy_target_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_target_parameterized, qoi_dist);
    
policy_global_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_global, qoi_dist);
  
POLICY_FNCS = {
    @policy_exploration
    @policy_max_var
    policy_extrema_kg
    %policy_target_kg
    %policy_global_kg
};

POLICY_LABELS = {
    'Exploration'
    'Max Variance'
    'KG (Learn extrema)'
    %'KG (Target y=1)'
    %'KG (Global)'
};

% ------------------------- Run simulations- ------------------------------
        
[truth, observations, mu, Sigma, choices, prior_samples] = simulate(...
    NUM_SIMS, NUM_SAMPLES_FOR_PRIORS, NUM_EXPERIMENTS, ...
    POLICY_FNCS, SIGMAW, generate_truth_parameterized, X);

% ------------------------ Calculate metrics ------------------------------

err = calc_perf(mu, truth, @perf_global_err);

% ---------------------------- Make plots ---------------------------------

for noise_idx=1:length(SIGMAW)
   make_perf_plots( squeeze(err(:,noise_idx,:,:)), POLICY_LABELS, ...
     sprintf('Global Error, \\sigma_w = %4.2f', SIGMAW(noise_idx)),...
     sprintf('global.sigmaw_%4.2f', SIGMAW(noise_idx)),...
     'Relative Error');
end

% plot animations
sim_idx = 1;
noise_idx = 1;
NUM_TO_SHOW = 1;

for policy_idx=1:length(POLICY_LABELS)
    animate_beliefs(X, ...
        squeeze(mu(sim_idx, noise_idx, policy_idx, :, :)), ...
        squeeze(Sigma(sim_idx, noise_idx, policy_idx, :, :, :)), ...
        squeeze(observations(sim_idx, noise_idx, :, :)), ...
        squeeze(choices(sim_idx, noise_idx, policy_idx, :)), ...
        truth(sim_idx, :), NUM_TO_SHOW, ...
        sprintf("%s.sim_idx__%03d.noise_idx__%02d", ...
        POLICY_LABELS{policy_idx}, sim_idx, noise_idx));
end

% save all simulation results
save('sim_results.mat', ...
    'truth', 'observations', 'mu', 'Sigma', 'choices', 'prior_samples',... 
    'NUM_SIMS', 'NUM_SAMPLES_FOR_PRIORS', 'NUM_EXPERIMENTS', ... 
    'NUM_MC_STEPS',  'SIGMAW', 'POLICY_LABELS', ...
    'MIN_X', 'MAX_X', 'STEP_X', ...
     '-v7.3');



function ret_val = generate_truth(X, min, max)
% uniformly sample parameters in interval [min, max]
J1 = rand()*(max-min) + min;
J2 = rand()*(max-min) + min;
ret_val = truth(X, J1, J2);
end


global kB
kB = 8.617330e-5;

% simulation loop sizes
NUM_SIMS = 10;
NUM_SAMPLES_FOR_PRIORS = 500;
NUM_EXPERIMENTS = 100;
NUM_MC_STEPS = 10;

% noise levels
SIGMAW = [0.1 .25 0.5];

% alternatives
MIN_X = 0;
MAX_X = 2000;
X_NUM_STEP = 50;
X = MIN_X:(MAX_X-MIN_X)/(X_NUM_STEP-1):MAX_X;

% truth generation
MIN_J = 0.01;
MAX_J = 0.05;

generate_truth_parameterized = @(X) ...
    generate_truth(X, MIN_J, MAX_J);

% Policies:

% units are index places, not temp
STEP_SIZE_IN_ORDER_POLICY = 5*X_NUM_STEP/NUM_EXPERIMENTS; 
policy_in_order_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_in_order(i, mu, Sigma, sigma2W, n, ...
    STEP_SIZE_IN_ORDER_POLICY);

policy_klla_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_klla(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS);

qoi_arg_target_parameterized = @(mu) ...
    qoi_arg_target(mu, 0.5);
qoi_dist = @(qoi1, qoi2) norm(qoi1 - qoi2);
policy_kg_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_arg_target_parameterized, qoi_dist);

POLICY_FNCS = {
    @policy_exploration
    @policy_max_var
    policy_kg_parameterized
    %policy_klla_parameterized
    policy_in_order_parameterized
};

POLICY_LABELS = {
    'Exploration'
    'Max Variance'
    'KG'
    %'KL Divergence'
    'In-order'
};

% ------------------------- Run simulations- ------------------------------
        
[truth, observations, mu, Sigma, choices, prior_samples] = simulate(...
    NUM_SIMS, NUM_SAMPLES_FOR_PRIORS, NUM_EXPERIMENTS, ...
    POLICY_FNCS, SIGMAW, generate_truth_parameterized, X);

% ------------------------ Calculate metrics ------------------------------

% define the performance metric based on how close we identify critical
% control variables (arg target)
perf_metric_arg_target = @(mu, truth) ...
    perf_normed_qoi_rel_err( mu, truth, qoi_arg_target_parameterized, @norm); 
perf_arg_target = calc_perf( mu, truth,  perf_metric_arg_target);

% calculate global error
perf_global_target = calc_perf( mu, truth, @perf_global_err);

% ---------------------------- Make plots ---------------------------------

for noise_idx=1:length(SIGMAW)
   make_perf_plots( squeeze(perf_arg_target(:,noise_idx,:,:)), ...
     POLICY_LABELS, ...
     sprintf('Selection Error, \\sigma_w = %4.2f', SIGMAW(noise_idx)),...
     sprintf('selection.sigmaw_%4.2f', SIGMAW(noise_idx)),...
     'Relative Error');
  
  make_perf_plots( squeeze(perf_global_target(:,noise_idx,:,:)), ...
     POLICY_LABELS, ...
     sprintf('Global Error, \\sigma_w = %4.2f', SIGMAW(noise_idx)),...
     sprintf('global.sigmaw_%4.2f', SIGMAW(noise_idx)),...
     'Relative Error');
end

% save all simulation results
save('sim_results.mat', ...
    'truth', 'observations', 'mu', 'Sigma', 'choices', 'prior_samples',... 
    'NUM_SIMS', 'NUM_SAMPLES_FOR_PRIORS', 'NUM_EXPERIMENTS', ... 
    'NUM_MC_STEPS',  'SIGMAW', 'POLICY_LABELS', ...
    'MIN_X', 'MAX_X', 'X_NUM_STEP', 'STEP_SIZE_IN_ORDER_POLICY', ...
    'MIN_J', 'MAX_J', '-v7.3');



function ret_val = truth(T, J1, J2)
    global kB
    beta = 1./(kB*T);
    k = 1./(sinh(2*J1*beta) .* sinh(2*J2*beta));
    ret_val = (1-k.^2).^(1/8).*(k<1);
    
end


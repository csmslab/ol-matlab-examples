clear;

% simulation loop sizes
NUM_SIMS = 100;
NUM_SAMPLES_FOR_PRIORS = 1000;
NUM_EXPERIMENTS = 50;
NUM_MC_STEPS = 20;

% noise levels
SIGMAW = [0.05 0.1 0.25 0.5];

% Define alternative set
MIN_X = 0;
MAX_X = 1;
STEP_X = 0.1;
NUM_X = (MAX_X-MIN_X)/STEP_X + 1;
X = cell(1, NUM_X);
count = 1;
for x1 = MIN_X:STEP_X:MAX_X
    for x2 = MIN_X:STEP_X:MAX_X
        X{count} = [x1, x2];
        count = count + 1;
    end
end

% Truth generation
A_LIM = [1, 10];
B_LIM = [0, 2];
C_LIM = [0.25, 0.75];
X_M_LIM = [0.75*MAX_X, MAX_X];
generate_truth_parameterized = @(X) ...
    generate_truth(X, A_LIM, B_LIM, C_LIM, X_M_LIM);

% Policies:

% KL-Divergence Policy
policy_klla_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_klla(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS);

% KG - target level set
qoi_level_curves_parameterized = @(mu) ...
    qoi_level_curves_2D(mu, 0.5, 0.01);
policy_level_curves_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_level_curves_parameterized, @qoi_dist_level_curve_2D);

% Global KG (maximize norm of difference between posterior and prior response function )
qoi_global = @(mu) mu;
policy_global_kg = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_global, @(qoi1, qoi2) norm(qoi1-qoi2));
  
POLICY_FNCS = {
    @policy_exploration
    @policy_max_var
    policy_klla_parameterized
    policy_global_kg
    policy_level_curves_kg
};

POLICY_LABELS = {
    'Exploration'
    'Max Variance'
    'KL Divergence'
    'KG (Global)'
    'KG (Level Curves)'
};

% ------------------------- Run simulations- ------------------------------
        
[truth, observations, mu, Sigma, choices, prior_samples] = simulate(...
    NUM_SIMS, NUM_SAMPLES_FOR_PRIORS, NUM_EXPERIMENTS, ...
    POLICY_FNCS, SIGMAW, generate_truth_parameterized, X);

% ------------------------ Calculate metrics ------------------------------

% Global error between truth and estimate
err = calc_perf(mu, truth, @perf_global_err);

% ---------------------------- Make plots ---------------------------------

for noise_idx=1:length(SIGMAW)
  make_perf_plots( squeeze(err(:,noise_idx,:,:)), POLICY_LABELS, ...
     sprintf('Global Error, \\sigma_w = %4.2f', SIGMAW(noise_idx)),...
     sprintf('global.sigmaw_%4.2f', SIGMAW(noise_idx)),...
     'Relative Error');
end

% Make plot of several phase diagrams for sanity check
figure();
colors = get(gca, 'ColorOrder');
for i=1:20
    [Y, x_m, x_s, a, b, c] = generate_truth_parameterized(X);
    %plot_2d_phase_diagram(Y', X, 0:0.01:1);
    %hold on;
    plot_defining_contour_functions(0:0.001:1, a, b, c, x_m, x_s, ...
        colors(mod((i-1), length(colors))+1,:));
end
set(gca, 'xlim', [MIN_X, MAX_X]);
set(gca, 'ylim', [MIN_X, MAX_X]);
set(gca, 'LineWidth', 1);
grid on;
xlabel('x_1');
ylabel('x_2');
set(gca, 'FontSize', 18);
saveas(gcf, 'phase_boundary_samples.pdf');

% save all simulation results
save('sim_results.mat', ...
    'truth', 'observations', 'mu', 'Sigma', 'choices', 'prior_samples',... 
    'NUM_SIMS', 'NUM_SAMPLES_FOR_PRIORS', 'NUM_EXPERIMENTS', ... 
    'NUM_MC_STEPS',  'SIGMAW', 'POLICY_LABELS', ...
    'MIN_X', 'MAX_X', 'STEP_X', ...
    'A_LIM', 'B_LIM', 'C_LIM', 'X_M_LIM', ...
     '-v7.3');

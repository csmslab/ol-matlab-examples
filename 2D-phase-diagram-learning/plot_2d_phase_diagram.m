function  plot_2d_phase_diagram(Y, X, XQ)
if((size(Y, 1) == 1) && (size(Y, 2) > 1))
error('Y must be a column vector');
end
    
X1_grid = zeros(length(X), 1);
X2_grid = zeros(length(X), 1);

for i=1:length(X)
    X1_grid(i) = X{i}(1);
    X2_grid(i) = X{i}(2);
end
F = scatteredInterpolant(X1_grid, X2_grid, Y, 'natural');
[X1_interp, X2_interp] = meshgrid(XQ, XQ);
Y_interp = F(X1_interp, X2_interp);
contour(X1_interp, X2_interp, Y_interp);

xlabel('X1');
ylabel('X2');
end


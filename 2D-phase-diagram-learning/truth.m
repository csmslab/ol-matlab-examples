% The phase at a specific point

function ret_val = truth(x, x_s, x_m, a, b, c)
x_1 = x(1);
x_2 = x(2);

y_1 = c + b*x_1.^2;
y_2 = a*(x_1 - x_m).^2;

ret_val = (x_2 < y_1).*(x_1 <= x_s) + ...
    (x_2 < y_2).*((x_1 > x_s)&(x_1 <= x_m));


end

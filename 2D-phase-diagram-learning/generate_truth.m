
% Generates a response surface over 2 dimensions based on phase diagrams
% The two dimension (X1, X2) are controllable variables, while the output function
% Y = F(X1,X2) describes phase or order parameter.

function [ret_val, x_m, x_s, a, b, c] = generate_truth(X, a_lim, b_lim, ...
    c_lim, x_lim)

% Confirm that X is a cell array of alternatives
if(~iscell(X))
    error('X must be a cell array of alternatives');
end

% sample coefficients until intersection point is well positioned
x_s = -1;

while(x_s < 0)
    % sample coefficients
    x_m = sample_from_interval(x_lim);
    a = sample_from_interval(a_lim);
    b = sample_from_interval(b_lim);
    c = sample_from_interval(c_lim);
    
    % solve for intersection points
    D =  sqrt(4*a^2*x_m^2 - 4*(a-b)*(a*x_m^2 - c));
    x_s1 = (2*a*x_m + D)/(2*(a - b));
    x_s2 = (2*a*x_m - D)/(2*(a - b));
    
    % find the solution between 0 and x_m
    if((x_s1 > 0) && (x_s1 < x_m))
        if((x_s2 > 0) && (x_s2 < x_m))
            % both are in interval
            if(rand() < 0.5)
                x_s = x_s1;
            else
                x_s = x_s2;
            end
        else
            % only x_s1 is in inteval
            x_s = x_s1;
        end
    else
        if((x_s2 > 0) && (x_s2 < x_m))
            % only x_s2 is in interval
            x_s = x_s2;
        else
            % neither are in interval
            x_s = -1;
        end
    end
end
    
% calculate phase diagram at each point
f = @(x) truth(x, x_s, x_m, a, b, c);
ret_val = cellfun(f, X);

end

% uniformly sample from 1D interval
function ret_val = sample_from_interval(x_lim)
x_min = x_lim(1);
x_max = x_lim(2);
ret_val = x_min + rand()*(x_max-x_min);
end
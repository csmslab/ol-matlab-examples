% QOI_DIST_LEVEL_CURVE_2D calculates the difference between 2 level curves

function ret_val = qoi_dist_level_curve_2D( qoi1, qoi2, X)

% bit difference
ret_val = norm(qoi1-qoi2);

end


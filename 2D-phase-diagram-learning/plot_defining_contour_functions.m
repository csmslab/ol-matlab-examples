
function plot_defining_contour_functions(x_1, a, b, c, x_m, x_s, color)
    
y_1 = c + b*x_1.^2;
y_2 = a*(x_1 - x_m).^2;

I1 = (x_1 <= x_s);
I2 = (x_1 >= x_s)&(x_1 <= x_m);
I3 = (x_1 >= x_m);

if(exist('color', 'var'))
    c1 = color;
    c2 = color;
else
    c1 = 'r';
    c2 = 'b';
end

h = plot(x_1(I1), y_1(I1));
set(h, 'Color', c1);
set(h, 'LineWidth', 2);
hold on;
h = plot(x_1(I2), y_2(I2));
set(h, 'Color', c2);
set(h, 'LineWidth', 2);
h = plot(x_1(I3), zeros(size(x_1(I3))));
set(h, 'Color', c2);
set(h, 'LineWidth', 2);

if(exist('color', 'var') == 0)
    plot(x_1(I2 | I3), y_1(I2 | I3), 'r:');
    plot(x_1(I1), y_2(I1), 'b:');
    plot(x_1(I3), y_2(I3), 'b:');
end

end


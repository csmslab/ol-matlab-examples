% Generates truth using a gaussian mixture model using the following
% parameters:
% X: the support for the model
% mu_n: the mean value used to sample the number of gaussians to use
% sigma_n: the std used to sample the number of gaussians to use
% mu_w: the mean value used to sample gaussian width
% sigma_w: the std used to sample gaussian width
% mu_h: the mean value used to sample gaussian height
% sigma_h: the std used to sample gaussian height
%
% Most parameters in a gaussian mixture model are positive. To sample
% positive parameters, we first sample from the appropriate guassian
% distribution, then take absolute values.

function ret_val = generate_truth(X, mu_n, sigma_n, mu_w, sigma_w, ...
    mu_h, sigma_h, target)

ret_val = zeros(size(X));

while( 2.0*target > max(ret_val))
    x_min = min(X);
    x_max = max(X);
    
    % sample the number of modes
    n = abs(mu_n + sigma_n*randn());
    
    % add modes
    for i=1:n
        % uniformly place center of mode
        c = x_min + rand()*(x_max - x_min);
        
        % sample width and height
        w = abs(mu_w + sigma_w*randn());
        h = abs(mu_h + sigma_h*randn());
        
        % add mode to ret_val
        ret_val = ret_val + h*exp(-(X-c).^2/(2*w^2));
        
    end
end

end
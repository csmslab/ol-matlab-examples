% This script performs simulations to test the targeting modality of
% optimal learning, in which we wish to learn the alternative whose
% corresponding response function value is closest to some pre-specified
% value.

% simulation loop sizes
NUM_SIMS = 500;
NUM_SAMPLES_FOR_PRIORS = 500;
NUM_EXPERIMENTS = 50;
NUM_MC_STEPS = 10;

% noise levels
SIGMAW = [1 2 4 6];

% target value
TARGET = 10;

% alternatives
MIN_X = 0;
MAX_X = 10;
STEP_X = 0.5;
X = MIN_X:STEP_X:MAX_X;

% truth generation
MU_NUM_MODES = 10;
SIG_NUM_MODES = 3;
MU_MODE_WIDTH = 0.5;
SIG_MODE_WIDTH = 0.25;
MU_MODE_HEIGHT = 5;
SIG_MODE_HEIGHT = 2;

generate_truth_parameterized = @(X) ...
    generate_truth(X, MU_NUM_MODES, SIG_NUM_MODES, ...
            MU_MODE_WIDTH, SIG_MODE_WIDTH, ...
            MU_MODE_HEIGHT, SIG_MODE_HEIGHT, TARGET);

% Policies:
policy_klla_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_klla(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS);
qoi_arg_target_parameterized = @(mu) ...
    qoi_arg_target(mu, TARGET);
policy_kg_parameterized = @(i, mu, Sigma, sigma2W, n) ...
    policy_kg(i, mu, Sigma, sigma2W, n, NUM_MC_STEPS, ...
        qoi_arg_target_parameterized, @(qoi1, qoi2) norm(qoi1-qoi2));

POLICY_FNCS = {
    @policy_exploration
    @policy_max_var
    policy_kg_parameterized
    %policy_klla_parameterized
};

POLICY_LABELS = {
    'Exploration'
    'Max Variance'
    'KG'
    %'KL Divergence'
};

% ------------------------- Run simulations- ------------------------------
        
[truth, observations, mu, Sigma, choices, prior_samples] = simulate(...
    NUM_SIMS, NUM_SAMPLES_FOR_PRIORS, NUM_EXPERIMENTS, ...
    POLICY_FNCS, SIGMAW, generate_truth_parameterized, X);

% ------------------------ Calculate metrics ------------------------------

% define the performance metric based on how close we identify critical
% control variables (arg target)
perf_metric_arg_target = @(mu, truth) ...
    perf_normed_qoi_rel_err( mu, truth, qoi_arg_target_parameterized, @norm); 
perf_arg_target = calc_perf( mu, truth,  perf_metric_arg_target);

% calculate global error
perf_global_target = calc_perf( mu, truth, @perf_global_err);

% ---------------------------- Make plots ---------------------------------

for noise_idx=1:length(SIGMAW)
   make_perf_plots( squeeze(perf_arg_target(:,noise_idx,:,:)), ...
     POLICY_LABELS, ...
     sprintf('Selection Error, \\sigma_w = %4.2f', SIGMAW(noise_idx)),...
     sprintf('selection.sigmaw_%4.2f', SIGMAW(noise_idx)),...
     'Relative Error');
  
  make_perf_plots( squeeze(perf_global_target(:,noise_idx,:,:)), ...
     POLICY_LABELS, ...
     sprintf('Global Error, \\sigma_w = %4.2f', SIGMAW(noise_idx)),...
     sprintf('global.sigmaw_%4.2f', SIGMAW(noise_idx)),...
     'Relative Error');
end

% plot animations
sim_idx = 1;
noise_idx = 1;
NUM_TO_SHOW = 1;

for policy_idx=1:length(POLICY_LABELS)
    animate_beliefs(X, ...
        squeeze(mu(sim_idx, noise_idx, policy_idx, :, :)), ...
        squeeze(Sigma(sim_idx, noise_idx, policy_idx, :, :, :)), ...
        squeeze(observations(sim_idx, noise_idx, :, :)), ...
        squeeze(choices(sim_idx, noise_idx, policy_idx, :)), ...
        truth(sim_idx, :), NUM_TO_SHOW, ...
        sprintf("%s.sim_idx__%03d.noise_idx__%02d", ...
        POLICY_LABELS{policy_idx}, sim_idx, noise_idx));
end

% save all simulation results
save('sim_results.mat', ...
    'truth', 'observations', 'mu', 'Sigma', 'choices', 'prior_samples',... 
    'NUM_SIMS', 'NUM_SAMPLES_FOR_PRIORS', 'NUM_EXPERIMENTS', ... 
    'NUM_MC_STEPS',  'SIGMAW', 'POLICY_LABELS', ...
    'MU_NUM_MODES', 'SIG_NUM_MODES', 'MU_MODE_WIDTH', 'SIG_MODE_WIDTH', ...
    'MIN_X', 'MAX_X', 'STEP_X', ...
    'MU_MODE_HEIGHT', 'SIG_MODE_HEIGHT', '-v7.3');
